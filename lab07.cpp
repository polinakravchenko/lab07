﻿// lab07.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

using namespace std;

int main()
{
    double y;
    double x;

    for (x = -1; x <= 1; x = x + 0.2)
    {
        y = 4.5 * pow(x, 2) + 10 * (x - 35);
        cout << " x: " << x << " y: " << y << endl;
    }
    return 0;
}